classdef SquareSignal < Block
    properties
        hi
        lo
        f
        Ts
        duty
    end
    methods
        function c = SquareSignal(out,hi,lo,f,duty)
            c = c@Block();
            c.hi = hi;
            c.lo = lo;     
            c.f = f;
            c.Ts = 1/f;
            c.duty = duty;
            c.noutput = 1;
            c.outpos(1) = out;
        end 
        
        function flag = Step(c,t,dt)
            d = t/c.Ts-floor(t/c.Ts);
            if d > c.duty
                  c.y = c.lo;
            else
                  c.y = c.hi;
            end
            flag = 1;
        end
        
    end
end
            
            
            
        
