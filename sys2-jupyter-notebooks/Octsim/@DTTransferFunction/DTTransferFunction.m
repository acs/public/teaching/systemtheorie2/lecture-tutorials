classdef DTTransferFunction < DTBlock
    properties
        A
        B
        C
        D
    end
    methods
        function c = DTTransferFunction(in,out,num,den,Ts)
            c = c@DTBlock();
            c.ninput = 1;
            c.noutput = 1;
            c.inpos(1) = in;
            c.outpos(1) = out;
	        c.Ts = Ts;
            sys = tf(num,den,Ts);
            sysss= ss(sys);
            c.A = sysss.A;
            c.B = sysss.B;
            c.C = sysss.C;
            c.D = sysss.D;
            c.nstate = size(c.A,1);
            c.x = zeros(c.nstate,1);
        end 
        
       function updatediscrete(c)
          c.y = c.C*c.x+c.D*c.u';
          c.x=c.A*c.x+c.B*c.u';      
          flag = 1;
       end
       
       function flag = Reset(b)
           b.x = zeros(c.nstate,1);
           flag = 1;
        end
             
    end
end
