classdef IdealDCVoltageSource < RCComponent
    properties
        Vs
        possource
    end
    methods
        function b = IdealDCVoltageSource(p,n,V)
            b = b@RCComponent(p,n);
            b.Vs = V;         
        end
        
        function [G,Bout] = ApplyMatrixStamp(b,P,dt)
            ntot = P.GetSize();
            b.possource = ntot+1;
            P.G =  [P.G,zeros(ntot,1)];
            P.G =  [P.G; zeros(1,ntot+1)];
            P.b =  [P.b; 0];  
            if (b.Pos > 0)
                P.G(b.possource,b.Pos) = 1;
                P.G(b.Pos, b.possource) = 1;
            end
            if (b.Neg > 0)    
                P.G(b.possource,b.Neg) = -1;
                P.G(b.Neg,b.possource) = -1;
            end
        end
        
        function Init(b,P,dt)
            b.ApplyMatrixStamp(P,dt);         
        end
        
        function Step(b,P,dt,t)
            P.b(b.possource) = b.Vs;
        end
        
        function PostStep(b,vsol,dt)
        end
    end
end
            
            
            
        
