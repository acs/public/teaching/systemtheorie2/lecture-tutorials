classdef Integrator < Block
    properties
        A
        B
        C
        D
    end
    methods
        function c = Integrator(in,out,ini)
            c = c@Block();
	    c.A = 0;
            c.B = 1;
            c.C = 1;
            c.D = 0;
            c.ninput = 1
            c.noutput = 1;
            c.inpos(1) = in;
            c.outpos(1) = out;
	    c.nstate = 1;
	    c.x = ini;
         end 
        
     function flag = Step(c,t,dt)
            c.y = c.C*c.x;
            c.updatestate(t,dt);      
            flag = 1;
        end
        
        function [dxdt] = cdxdt(c,x,t)
            dxdt = c.A*x+c.B*c.u;
        end
        
        
    end
end
            
            
            
        
