import numpy as np


class Schema(object):
    def __init__(self, ti,tf,dt,numflows):
        self.tini = ti    
        self.tfin = tf    
        self.nmodels = 0
        self.nflows = numflows
        self.dt = dt
        self.flows = np.zeros(numflows)
        self.BlockList = list()
        
    def AddComponent(self,h):
        self.nmodels = len(self.BlockList)+1
        self.BlockList.append(h)

    def AddListComponents(self,l):
        p = len(l)
        for i in range(0,p):
            self.BlockList.append(l[i])
            self.nmodels = self.nmodels+1

    def Init(self):
        self.t = self.tini
        for i in range(0,self.nflows):
            self.flows[i] = 0

    def Reset(self):
        for i in range(0,self.nmodels):
            self.BlockList[i].Reset()

    def Run(self,listout):
        self.Init()
        qmax = listout.size            
        npoints = (self.tfin-self.tini)/self.dt
        npt = int(npoints)
        out = np.zeros((qmax+1,npt-1))
        p=0
        while self.Step() and p<npt-1:
            out[0,p] = self.GetTime()
            for q in range(0,qmax):
                out[q+1, p] = self.GetFlow(listout[q])
            p=p+1
        return out
            
    def ChangeTimeStep(self,dt):
        self.dt = dt

    def ChangeParameters(self,pos,val):
        self.ModelList[pos].ChangeParameters(val)
        
    def Step(self):
        for  i in range(0,self.nmodels): 
            self.BlockList[i].GetInput(self)
            self.BlockList[i].Step(self.t,self.dt)
            self.BlockList[i].WriteOutput(self)
        self.t = self.t+self.dt
        if self.t < self.tfin:
                flag = 1
        else:
                flag = 0
        return flag    
    
    def GetFlow(self,n):
        val = self.flows[n]
        return val     
           
    def GetTime(self):
        val = self.t
        return val
  