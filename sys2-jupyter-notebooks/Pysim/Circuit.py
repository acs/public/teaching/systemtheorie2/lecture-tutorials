import numpy


class LinProb(object):
    def __init__(self, nodeNum):        
        self.G = numpy.zeros((nodeNum,nodeNum), dtype=numpy.complex)
        self.b = numpy.zeros((nodeNum,1), dtype=numpy.complex)

    def getSize(self):
        return self.G.shape[0]
        
    def resetbVector(self):        
        self.b = numpy.zeros((self.b.shape[0],1), dtype=numpy.complex)

    def solve(self):
        result = numpy.dot(numpy.linalg.inv(self.G), self.b)
        return result
        
    def show(self):
        print(self.G)
        print(self.b)


class Circuit(object):
    def __init__(self, nnode):
        self.n = nnode      
        self.NetList = list()
        
    def addComponent(self,h):
        print("adding component" + str(h))
        self.nelements = len(self.NetList)+1
        self.NetList.append(h)
        
    def initialize(self,dt):
        self.Pn = LinProb(self.n)
        self.Pn.show()
        self.nelements = len(self.NetList)        
        print("number of components " + str(self.nelements))
        for i in range(0,self.nelements):
            print("initializing component " + str(i+1))
            self.NetList[i].initialize(self.Pn,dt)
            
        return self.Pn
    
    def initializeDp(self,dt,om):
        self.Pn = LinProb(self.n)
        self.nelements = len(self.NetList)
        for i in range(0,self.nelements):
            self.NetList[i].initialize(self.Pn,dt,om)
            
    def solveLinTrans(self,tini,tfin,dt):
        print("show problem")
        self.Pn.show()        
        t = tini
        k = 0
        
        while t < tfin:
            #print("solving for t = " + str(t))
            self.Pn.resetbVector()            
            
            for i in range(0,self.nelements):
                self.Pn = self.NetList[i].step(self.Pn,dt,t)
                
            if t == 0:
                self.vout = self.Pn.solve()
            else:
                self.vout = numpy.append(self.vout, self.Pn.solve(), 1)            
            
            for i in range(0,self.nelements):
                self.NetList[i].postStep(self.vout[:,k],dt)
                
            t = t+dt
            k = k+1
            
        return self.vout
