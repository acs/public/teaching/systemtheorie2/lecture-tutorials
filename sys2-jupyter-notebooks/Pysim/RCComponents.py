import numpy


class RCComponent(object):
    def __init__(self, p, n):
        self.Pos = p
        self.Neg = n
        self.linear = 1
        self.hyin = 0
        self.hyout = 0
        
    def applyMatrixStamp(self, P,dt):
        return P
        
    def initialize(self, P,dt): 
        return P     
        
    def step(self,P,dt,t):
        return P 

    def postStep(self,vsol,dt):
        return vsol


class Resistance(RCComponent):           
    def __init__(self,p,n,par):      
        super(Resistance, self).__init__(p,n)
        self.r = par        
        if par > 0:
            self.Gc = 1/par
        else:
            print("Error: Resistance value not correct")
            
    def initialize(self,P,dt):
        print("initializing resistance")
        return Resistance.applyMatrixStamp(self,P,dt)
            
    def applyMatrixStamp(self,P,dt):  
        if self.Pos > -1:
            P.G[self.Pos,self.Pos] = P.G[self.Pos,self.Pos] + self.Gc
        if self.Neg > -1:
            P.G[self.Neg,self.Neg] = P.G[self.Neg,self.Neg] + self.Gc
        if (self.Pos > -1) and (self.Neg > -1):
            P.G[self.Pos,self.Neg] = P.G[self.Pos,self.Neg] - self.Gc
            P.G[self.Neg,self.Pos] = P.G[self.Neg,self.Pos] - self.Gc  
        P.show()              
        return P         


class Capacitor(RCComponent):
    def __init__(self,p,n,par):
        super(Capacitor, self).__init__(p,n)
        self.C = par
        self.Gl = 0
        
    def initialize(self,P,dt):
        print("initializing capacitor")
        self.Bc = 0
        self.Vc = 0
        self.Ic = 0
        return Capacitor.applyMatrixStamp(self,P,dt)
        
    def applyMatrixStamp(self,P,dt): 
        self.Gc = 2*self.C/dt      
        if self.Pos > -1:
            P.G[self.Pos,self.Pos] = P.G[self.Pos,self.Pos] + self.Gc
        if self.Neg > -1:
            P.G[self.Neg,self.Neg] = P.G[self.Neg,self.Neg] + self.Gc
        if (self.Pos > -1) and (self.Neg > -1):
            P.G[self.Pos,self.Neg] = P.G[self.Pos,self.Neg] - self.Gc
            P.G[self.Neg,self.Pos] = P.G[self.Neg,self.Pos] - self.Gc      
            
        P.show()   
        return P      
        
    def step(self,P,dt,t):
        self.Bc = self.Gc*self.Vc+self.Ic
        if self.Pos > -1:
            P.b[self.Pos] = P.b[self.Pos] + self.Bc
        if self.Neg > -1:
            P.b[self.Neg] = P.b[self.Neg] - self.Bc
        return P
        
    def postStep(self,vsol,dt):
        if self.Pos > -1:
            if self.Neg > -1:
                self.Vc = vsol[self.Pos] - vsol[self.Neg]
            else:
                self.Vc = vsol[self.Pos]              
        else:
            self.Vc = -vsol[self.Neg];    
                    
        self.Ic = self.Gc*self.Vc-self.Bc;


class Inductor(RCComponent):    
    def __init__(self,p,n,par):
        super(Inductor, self).__init__(p,n)
        self.L = par
        
    def initialize(self,P,dt):        
        print("initializing inductor")
        self.Bl = 0
        self.Vl = 0
        self.Il = 0
        return Inductor.applyMatrixStamp(self,P,dt)
        
    def applyMatrixStamp(self,P,dt): 
        self.Gl = dt/(2*self.L)      
        if self.Pos > -1:
            P.G[self.Pos,self.Pos] = P.G[self.Pos,self.Pos] + self.Gl
        if self.Neg > -1:
            P.G[self.Neg,self.Neg] = P.G[self.Neg,self.Neg] + self.Gl
        if (self.Pos > -1) and (self.Neg > -1):
            P.G[self.Pos,self.Neg] = P.G[self.Pos,self.Neg] - self.Gl
            P.G[self.Neg,self.Pos] = P.G[self.Neg,self.Pos] - self.Gl   
        P.show()             
        return P       
        
    def step(self,P,dt,t):
        self.Bl = self.Gl*self.Vl+self.Il
        if self.Pos > -1:
            P.b[self.Pos] = P.b[self.Pos] - self.Bl
        if self.Neg > -1:
            P.b[self.Neg] = P.b[self.Neg] + self.Bl
        return P
                
    def postStep(self,vsol,dt):
        if self.Pos > -1:
            if self.Neg > -1:
                self.Vl = vsol[self.Pos] - vsol[self.Neg]
            else:
                self.Vl = vsol[self.Pos]              
        else:
            self.Vl = -vsol[self.Neg];    
                    
        self.Il = self.Gl*self.Vl+self.Bl;

  
class IdealACVoltageSource(RCComponent):    
    def __init__(self,p,n,V,omega,phase):
        super(IdealACVoltageSource, self).__init__(p,n)
        self.Vs = V
        self.om = omega
        self.ph = phase
        
    def initialize(self,P,dt):
        print("initializing ideal ac voltage source")
        return IdealACVoltageSource.applyMatrixStamp(self,P,dt)
        
    def applyMatrixStamp(self,P,dt):         
        self.ntot = P.getSize()
        self.possource = self.ntot
        print("position source at " + str(self.possource))
        P.G = numpy.append(P.G, numpy.zeros((self.ntot,1), dtype=numpy.complex), 1)
        P.G = numpy.append(P.G, numpy.zeros((1,self.ntot+1), dtype=numpy.complex), 0)
        P.b = numpy.append(P.b, numpy.zeros((1,1), dtype=numpy.complex), 0)
                
        if self.Pos > -1:
            P.G[self.possource,self.Pos] = 1
            P.G[self.Pos,self.possource] = 1
        if self.Neg > -1:
            P.G[self.possource,self.Neg] = -1
            P.G[self.Neg,self.possource] = -1  
            
        P.show()
        return P       
            
    def step(self,P,dt,t):
        P.b[self.possource,0] = self.Vs * numpy.sin(self.om*t+self.ph)
        return P
