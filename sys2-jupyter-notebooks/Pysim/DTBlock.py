import numpy
try:
    from .Block import Block
except:
    from Block import Block
inv = numpy.linalg.inv
dot = numpy.dot


#########################################################################
#### Abstract class for discrete time block in a block diagram ########
#########################################################################

class DTBlock(Block):
    def __init__(self):
        self.ninput = 0    
        self.noutput = 0    
        self.nstate = 0
        self.nparam = 0
        self.inpos = numpy.zeros((self.ninput),dtype=numpy.int)
        self.outpos = numpy.zeros((self.noutput),dtype=numpy.int)
        self.x = numpy.zeros((self.nstate))
        self.xo = numpy.zeros((self.nstate))
        self.u = numpy.zeros((self.ninput))
        self.y = numpy.zeros((self.noutput))    
        self.Ts = 0.001
        self.lastt = -10
        
    def Step(self,t,dt):
        if t-self.lastt >= self.Ts:
            self.updatediscrete()
            self.lastt = t
                     
    def updatediscrete(self):
        self.y[0] = 0
        
    def Reset(self):
        self.lastt = -10;


#########################################################################
################################ FIR ####################################
#########################################################################

class FIR(DTBlock):
    def __init__(self,inp,out,a,ts):
            super(FIR, self).__init__()
            self.ninput = 1
            self.inpos = numpy.zeros((self.ninput),dtype=numpy.int)
            self.inpos[0] = inp            
            self.noutput = 1
            self.outpos = numpy.zeros((self.noutput),dtype=numpy.int)
            self.outpos[0] = out
            self.Ts = ts
            self.pt = len(a)
            self.a = numpy.zeros((self.pt))
            self.a = a
            self.buf = numpy.zeros((self.pt))  
            self.u = numpy.zeros((self.ninput)) 
            self.y = numpy.zeros((self.noutput))   
            self.y[0] = 0
                    
    def updatediscrete(self):
            for i in range (self.pt-1,0,-1):
                self.buf[i] = self.buf[i-1]
            self.buf[0] = self.u[0]
            self.y[0] = 0
            for i in range (0,self.pt):
                self.y[0] = self.y[0]+self.a[i]*self.buf[i]      
           
    def Reset(self):
        self.buf = zeros(self.pt,1)
        self.lastt = -10


#########################################################################
#########################Discrete Integral###############################
#########################################################################

class DTIntegral(DTBlock):
    def __init__(self,inp,out,ts,xo):
            super(DTIntegral, self).__init__()
            self.ninput = 1
            self.inpos = numpy.zeros((self.ninput),dtype=numpy.int)
            self.inpos[0] = inp            
            self.noutput = 1
            self.outpos = numpy.zeros((self.noutput),dtype=numpy.int)
            self.outpos[0] = out
            self.Ts = ts
            self.xo = xo
            self.x = xo
            self.u = numpy.zeros((self.ninput)) 
            self.y = numpy.zeros((self.noutput))   
            self.y[0] = xo
                    
    def updatediscrete(self):
           self.y[0] = self.x
           self.x = self.x + self.u[0]*self.Ts
           
    def Reset(self):
        self.x = self.xo  


#########################################################################
#########################   ZOH         #################################
#########################################################################

class ZOH(DTBlock):
    def __init__(self,inp,out,ts):
            super(ZOH, self).__init__()
            self.ninput = 1
            self.inpos = numpy.zeros((self.ninput),dtype=numpy.int)
            self.inpos[0] = inp            
            self.noutput = 1
            self.outpos = numpy.zeros((self.noutput),dtype=numpy.int)
            self.outpos[0] = out
            self.Ts = ts
            self.u = numpy.zeros((self.ninput)) 
            self.y = numpy.zeros((self.noutput))   
            self.y[0] = 0
                    
    def updatediscrete(self):
           self.y[0] = self.u[0]


#########################################################################
#######################   Delay         #################################
#########################################################################

class DTDelay(DTBlock):
    def __init__(self,inp,out,init,ts):
        super(DTDelay, self).__init__()
        self.ninput = 1
        self.inpos = numpy.zeros((self.ninput),dtype=numpy.int)
        self.inpos[0] = inp            
        self.noutput = 1
        self.outpos = numpy.zeros((self.noutput),dtype=numpy.int)
        self.outpos[0] = out
        self.Ts = ts
        self.u = numpy.zeros((self.ninput)) 
        self.y = numpy.zeros((self.noutput))   
        self.y[0] = 0
        self.oldv = init
        self.init = init
                    
    def updatediscrete(self):
        self.y[0] = self.oldv
        self.oldv = self.u[0]

    def ChangeParameters(self,value):
        self.Ts = value
            
    def Reset(self):
        self.oldv = self.init


#########################################################################
################################ IIR ####################################
#########################################################################

class IIR(DTBlock):
    def __init__(self,inp,out,a,b,ts):
            super(IIR, self).__init__()
            self.ninput = 1
            self.inpos = numpy.zeros((self.ninput),dtype=numpy.int)
            self.inpos[0] = inp            
            self.noutput = 1
            self.outpos = numpy.zeros((self.noutput),dtype=numpy.int)
            self.outpos[0] = out
            self.Ts = ts
            self.pn = len(a)
            self.a = numpy.zeros((self.pn))
            self.a = a
            self.pd = len(b)
            self.b = numpy.zeros((self.pd))
            self.b = b
            self.bufn = numpy.zeros((self.pn))
            self.bufd = numpy.zeros((self.pd))
            self.u = numpy.zeros((self.ninput)) 
            self.y = numpy.zeros((self.noutput))   
            self.y[0] = 0
                    
    def updatediscrete(self):
            for i in range (self.pn-1,0,-1):
                self.bufn[i] = self.bufn[i-1]
            self.bufn[0] = self.u[0]
            self.y[0] = 0
            for i in range (0,self.pn):
                self.y[0] = self.y[0]+self.a[i]*self.bufn[i]               
            for i in range (self.pd-1,0,-1):
                self.bufd[i] = self.bufd[i-1]
            self.bufd[0] = self.y[0]
            for i in range (0,self.pd):
                self.y[0] = self.y[0]-self.b[i]*self.bufd[i]      
    
    def Reset(self):
        self.bufn = zeros(self.pn,1)
        self.bufd = zeros(self.pd,1)
        self.lastt = -10 


#########################################################################
############### Discrete Time Transfer Function  ########################
#########################################################################

class DTTransferFunction(DTBlock):
    def __init__(self,inp,out,num,den,Ts):                
        super(DTTransferFunction, self).__init__()
        self.Ts = Ts
        self.ninput = 1
        self.noutput = 1
        self.inpos = numpy.zeros((self.ninput),dtype=numpy.int)
        self.outpos = numpy.zeros((self.noutput),dtype=numpy.int)
        self.inpos[0] = inp
        self.outpos[0] = out
        self.nstate = len(den)-1
        denord = len(den)
        numord = len(num)
        self.A = numpy.zeros((self.nstate,self.nstate))
        self.B = numpy.zeros((self.nstate))
        self.C = numpy.zeros((self.nstate))
        self.D = 0 
        scale = den[0];
        for i in range(0,denord):
            den[i]= den[i]/scale
        for i in range(0,numord):
            num[i]= num[i]/scale
        if numord==denord:
            self.D = num[0]
            for i in range(0,numord-1):
                self.C[i]=num[numord-i-1]-num[0]*den[numord-i-1]
        else:
            for i in range(0,numord):
                self.C[i]=num[numord-i-1]
        for i in range(0,self.nstate):
            self.A[self.nstate-1,i] = -den[denord-i-1]
        for i in range(0,self.nstate-1):
            self.A[i,i+1]=1
        self.B[self.nstate-1] = 1
        self.x = numpy.zeros((self.nstate))
        self.u = numpy.zeros((self.ninput))
        self.y = numpy.zeros((self.noutput)) 
        for i in range(0,self.nstate):
            self.x[i] = 0  
        
    def updatediscrete(self):
        self.y[0] = self.D*self.u[0]
        for i in range (0,self.nstate):
            self.y[0] = self.y[0]+self.C[i]*self.x[i]
        xn = numpy.zeros((self.nstate)) 
        for i in range (0,self.nstate):
            xn[i] = self.B[i]*self.u[0]
            for j in range (0,self.nstate):
                xn[i] = xn[i] + self.A[i,j]*self.x[j]
        for i in range (0,self.nstate):
            self.x[i] = xn[i]
    
    def Reset(self):
        for i in range(0,self.nstate):
            self.x[i] = 0


#########################################################################
########################### DT State Space  #############################
#########################################################################

class DTStateSpace(DTBlock):
    def __init__(self,inp,out,A,B,C,D,xo,Ts):                
        super(DTStateSpace, self).__init__()
        self.Ts = Ts
        self.ninput = len(inp)
        self.noutput = len(out)
        self.nstate = len(A)
        self.A = A
        self.B = B
        self.C = C
        self.D = D
        self.inpos = inp
        self.outpos = out
        self.x = numpy.zeros((self.nstate))
        self.xo = numpy.zeros((self.nstate))
        self.u = numpy.zeros((self.ninput))
        self.y = numpy.zeros((self.noutput)) 
        for i in range(0,self.nstate):
            self.x[i] = xo[i]
            self.xo[i] = xo[i]
    
    def updatediscrete(self):
        for i in range(0,self.noutput):
            self.y[i] = 0
            for j in range(0,self.nstate):
                if self.C.ndim == 1:
                    self.y[i] = self.y[i] + self.C[j]*self.x[j]
                elif self.C.ndim == 2:
                    #prevent [ValueError: setting an array element with a sequence.] for 2 dimensional self.C:
                    self.y[i] = self.y[i] + self.C[i][j]*self.x[j]
                else:
                    print("WARNING: Can not handle self.C with dimension "+str(self.C.ndim))
            for j in range(0,self.ninput):
                if self.D.ndim == 1:
                    self.y[i] = self.y[i] + self.D[j]*self.u[j]
                elif self.D.ndim == 2:
                    #prevent [ValueError: setting an array element with a sequence.] for 2 dimensional self.C:
                    self.y[i] = self.y[i] + self.D[i][j]*self.u[j]
                else:
                    print("WARNING: Can not handle self.D with dimension "+str(self.C.ndim))
        self.x = numpy.matmul(self.B,self.u.T) + numpy.matmul(self.A,self.x)
    
    def Reset(self):
        for i in range(0,self.nstate):
            self.x[i] = 0


#########################################################################
########################### Kalman-Filter  ##############################
#########################################################################

class KalmanFilter(DTBlock):
    def __init__(self,inp,out,nin,nmeas,F,G,C,Q,R,xo,Pk,Ts):
        self.F = F
        self.G = G
        self.C = C
        self.Q = Q
        self.R = R
        self.x = xo
        self.Pk = Pk
        self.Ts = Ts
        self.nin = nin
        self.nmeas = nmeas
        self.ninput = len(inp)
        self.noutput = len(out)
        self.inpos = inp;
        self.outpos = out;
        self.nstate = len(F);
        self.u = numpy.zeros((self.ninput))
        self.y = numpy.zeros((self.noutput))
        self.lastt = -10  
    
    def updatediscrete(self):
        uu = numpy.zeros((self.nstate))
        m = numpy.zeros((self.nstate))
        if (self.nin != 0):
            for i in range(0,self.nin):
                uu[i] = self.u[i];
        for i in range(0,self.nmeas):
            m[i] = self.u[self.nin+i]

        # Zustandsvorhersage:
        # x = F*x + G*u
        self.x = dot(self.F,self.x)+dot(self.G,uu)
        # Kovarianz der Zustandsvorhersage:
        # Pk = F*P*F.T + Q
        self.Pk = dot(self.F,dot(self.Pk,self.F.T)) + self.Q

        # Kovarianz der Innovation:
        # S = C*Pk*C.T + R
        S = dot(self.C, dot(self.Pk, self.C.T))+ self.R

        # Kalman Gain:
        # Kk = Pk*self.C.T*inv(S)
        Kk = dot(dot(self.Pk, self.C.T), inv(S))

        # Messvorhersage:
        # z = C*x
        z = dot(self.C,self.x)
        
        # Innovation:
        v = m - z
        
        # Kovarianzkorrektur:
        # Pk = (I-(Kk*C))*Pk
        self.Pk= dot((numpy.eye(self.nstate)-dot(Kk,self.C)),self.Pk)
        
        # Zustandskorrektur:
        self.x = self.x+dot(Kk,v)
        
        # Widergabe des Zustandsvektors:
        self.y = self.x

    def Reset(self):
        self.x = xo
        self.Pk = Pk
