disp('Definition des System');
A = [1 1;
    -2   -1]

B = [0; 1]

C = [1 0]

D = 0

disp('Steuerbarkeit des System');
S_c = [B  A*B]
rank(S_c)

disp('Eigenwerte');
eigs(A)

disp('Regler');

csi = 0.7;
omr = 2;

for i=1:10
    omo = i*omr;
    pd = [-csi*omo+1j*omo*sqrt(1-csi*csi)  -csi*omo-1j*omo*sqrt(1-csi*csi)];
    F = place(A,B,pd);
    xini = [2; 2];
    Acl = A-B*F;
    
    deltat=0.01;
    for q=1:100
        y(i,q) = C*expm(Acl*deltat*q)*xini;
        u(i,q) = -F*expm(Acl*deltat*q)*xini;
    end
end

figure
hold on
for i=1:10
    plot((1:100)*deltat,y(i,:));
end
hold off

figure
hold on
for i=1:10
    plot((1:100)*deltat,u(i,:));
end
hold off

