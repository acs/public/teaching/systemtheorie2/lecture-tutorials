disp('Definition des System');
A = [1 1;
    -2   -1]

B = [0; 1]

C = [1 0]

D = 0

disp('Steuerbarkeit des System');
W = [B  A*B]
rank(W)

disp('Eigenwerte');
eigs(A)

disp('Regler');
pd = [-3+1j*3  -3-1j*3]
F = place(A,B,pd)

V = inv(C*inv(B*F-A)*B)