disp('Definition des System');
A = [1 1;
    -2   -1]

B = [0; 1]

C = [1 0]

D = 0

%Pr�fe die Steuerbarkeit mittels Rang-Funktion
disp('Steuerbarkeit des System');
S_c = [B  A*B]
rank(S_c)

%Ermittle Eigenwerte f�r den offenen Regelkreis
disp('Eigenwerte');
eigs(A)
 
%Gebe nun f�r den geschlossenen Regelkreis diese zwei Pole vor
disp('Regler');
pd = [-3+1j*3  -3-1j*3]
F = place(A,B,pd)

