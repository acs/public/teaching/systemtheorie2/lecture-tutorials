p1 = -5
p2 = -8;

Num = [p1*p2]
Den = [1 -(p1+p2) p1*p2]

Gs = tf(Num,Den)

bode(Gs)
pause

% PID 
omo = 20

Kd = omo/(p1*p2)
Kp = Kd*(-(p1+p2))
Ki = Kd*p1*p2

Gr = tf([Kd Kp Ki],[1 0]);

Go = Gs*Gr

bode(Go)