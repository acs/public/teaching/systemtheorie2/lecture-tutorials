% System
A = [-2 1;
      0 -1]
  
B = [1; 1]

C = [1 0]

D = 0;

% Beobachtbarkeit/Observability:
So = [C; C*A]
rank(So)

%po = [-5+1j*5 -5-1j*5]  %Beobachterpole
poles = [-10+1j*5 -10-1j*5] %Dynamisches Verhalten des Beobachters �ndern
G = place(A',C',poles)

%Beobachter Eigenwerte:
Ao = A-G'*C
eigs(Ao)
