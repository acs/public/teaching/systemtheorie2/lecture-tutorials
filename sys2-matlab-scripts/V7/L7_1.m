% System definition
A = [-1 -1 -2; 0 -1 1; 1 0 -1];
b = [2; 0; 1];
c = [1 1 0];

% Controllability and Observability Matrix
S_C = [b A*b A*A*b];
S_O = [c; c*A; c*A*A];

% Check on rank
rank(S_C)
det(S_C)
rank(S_O)
det(S_O)
