% Example #6: 

% Step 1: Define variables with following values
    %num= [8  0];
    %den =[1 -3 2];
    %npoint = 20;

%Step 2: Call function longdiv to determine y
    %y = longdiv(num,den,npoint)

%Step 3: Plot y
    %plot(y)

function y = longdiv(num,den,npoint)
[y(1) R] = deconv(num,den);

for i=2:npoint
    num = conv(R,[1 0]);
    [f R] = deconv(num,den);
    y(i) = f(length(f));
end
end