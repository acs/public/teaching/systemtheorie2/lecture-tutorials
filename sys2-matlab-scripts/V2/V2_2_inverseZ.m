% Inverse z-Transformation - 2

N = [8 2 3 2 1]
D = [1 0 0 0 0]


[f0 R] = deconv(N,D)
pause

R = conv(R,[1 0])
[f1 R] = deconv(R,D)
pause

R = conv(R,[1 0])
[f2 R] = deconv(R,D)
pause

R = conv(R,[1 0])
[f3 R] = deconv(R,D)
pause

R = conv(R,[1 0])
[f4 R] = deconv(R,D)
pause

R = conv(R,[1 0])
[f5 R] = deconv(R,D)
pause
