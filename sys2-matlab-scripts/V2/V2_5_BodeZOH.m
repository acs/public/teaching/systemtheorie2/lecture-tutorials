function [G Ph] = BodeZOH(Ts,ommax,dom)
k=0;
kmax = round(ommax/dom);
for k=1:kmax
    om(k) = k*dom;
    Renum = 1 -cos(-om(k)*Ts);
    Imnum = -sin(-om(k)*Ts);
    Gnum = sqrt(Renum*Renum+Imnum*Imnum);
    G(k) = Gnum/(om(k)*Ts);
    Ph(k) = atan2(Imnum,Renum)-pi/2;
end

subplot(2,1,1), semilogx(om,20*log10(G));
subplot(2,1,2), semilogx(om,Ph*90/pi);

%Ts = 0.1
%ommax = 100
%dom = 0.1