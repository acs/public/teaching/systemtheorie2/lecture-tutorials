% Inverse z-Transformation

N = [8 0]
D = [1 -3 2]


[f0 R] = deconv(N,D)
pause

R = conv(R,[1 0])
[f1 R] = deconv(R,D)
pause

R = conv(R,[1 0])
[f2 R] = deconv(R,D)
pause

R = conv(R,[1 0])
[f3 R] = deconv(R,D)
pause
